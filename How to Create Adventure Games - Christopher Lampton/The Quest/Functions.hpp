#ifndef _FUNCTIONS_HPP
#define _FUNCTIONS_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <map>
using namespace std;

enum DIRECTIONS { NORTH = 0, SOUTH = 1, EAST = 2, WEST = 3, UP = 4, DOWN = 5, NONE };

enum FLAG_NAME { GLOVE_FLAG,
    SALT_POURED,
    FORMULA_POURED,
    BARREL_HAS_SALT, BARREL_HAS_WATER, BARREL_HAS_FORMULA,
    WINNABLE_STATE,
    TELEPORT_STATE
};
enum FLAG_VALUE {
    NOT_WEARING, WEARING,
    NOT_POURED, POURED,
    HAS, NOT_HAVE,
    WINNABLE, NOT_WINNABLE,
    TELEPORTED, NOT_TELEPORTED
};

bool IsItemInInventory( int objectInRoom[], int itemIndex )
{
    return ( objectInRoom[ itemIndex ] == -1 );
}

bool IsItemInRoom( int objectInRoom[], int itemIndex, int currentRoom )
{
    if ( objectInRoom[ itemIndex ] < 128 )
    {
        return ( objectInRoom[ itemIndex ] == currentRoom );
    }
    else
    {
        return ( (objectInRoom[ itemIndex ] - 128) == currentRoom );
    }
}

void SetToNotWinnable( map< FLAG_NAME, FLAG_VALUE >& gameFlags )
{
    gameFlags[ WINNABLE_STATE ] = NOT_WINNABLE;
    cout << "** GAME IS NOW NOT WINNABLE **" << endl;
}

string ToUpper( const string& val )
{
    string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += toupper( val[i] );
    }
    return upper;
}

// Line 92
void GetInput( string& verb, string& noun )
{
    string rawInput;
    cout << endl << ">> ";
    getline( cin, rawInput );

    int spacePos = rawInput.find( " " );
    verb = ToUpper( rawInput.substr( 0, spacePos ) );
    noun = ToUpper( rawInput.substr( spacePos+1 ) );
}

// Line 490
void PrintDirectionsYouCanGo( int mapNeighbors[19][6], const string directions[], const int numberOfRooms, const int numberOfDirections, const int currentRoom )
{
    cout << endl << "YOU CAN GO: ";
    for ( int i = 0; i < numberOfDirections; i++ )
    {
        if ( mapNeighbors[ currentRoom ][i] > 0 )
        {
            cout << directions[i] << " ";
        }
    }
}

// Line 600
void PrintWhatYouCanSee( string objectNames[], string objectTags[], int objectInRoom[], const int numberOfObjects, const int currentRoom )
{
    cout << endl << "YOU CAN SEE: ";

    bool foundSomething = false;
    string canSee = "";

    for ( int i = 1; i <= numberOfObjects; i++ )
    {
        if ( objectInRoom[i] != 0 && IsItemInRoom( objectInRoom, i, currentRoom ) )
        {
            cout << endl << "- " << objectNames[i] << " (" << objectTags[i] << ") ";
            foundSomething = true;
        }
    }

    if ( !foundSomething )
    {
        cout << " NOTHING OF INTEREST";
    }
    cout << endl;
}

// Line 691
void PrintRoomDescription( string roomNames[], const int currentRoom )
{
    cout << "YOU ARE " << roomNames[currentRoom] << endl;
}

// Line 1998
void HandleGoCommand( int mapNeighbors[19][6], const string direction[6], int objectInRoom[], int& currentRoom, const string verb, const string noun )
{
    int intendedDirection = NONE; // DI

    if ( noun == "NORTH" || noun == "NOR" || noun == "N" )
    {
        intendedDirection = NORTH;
    }
    else if ( noun == "SOUTH" || noun == "SOU" || noun == "S" )
    {
        intendedDirection = SOUTH;
    }
    else if ( noun == "EAST" || noun == "EAS" || noun == "E" )
    {
        intendedDirection = EAST;
    }
    else if ( noun == "WEST" || noun == "WES" || noun == "W" )
    {
        intendedDirection = WEST;
    }
    else if ( noun == "BOA" || noun == "BOAT" ) // Line 2070
    {
        // Is the boat on this map? Gotta do some funky math!
        if ( objectInRoom[12] - 128 == currentRoom )
        {
            currentRoom = 13;
            cout << "YOU USED THE BOAT" << endl;
            return;
        }
    }
    else if ( noun == "DOWN" || noun == "DOW" )
    {
        intendedDirection = DOWN;
    }
    else if ( noun == "UP" )
    {
        intendedDirection = UP;
    }
    else
    {
        cout << "YOU CAN'T GO THERE!" << endl;
        return;
    }

    if (    mapNeighbors[ currentRoom ][ intendedDirection ] > 0 &&
            mapNeighbors[ currentRoom ][ intendedDirection ] < 128 )
    {
        cout << endl << "YOU GO " << direction[ intendedDirection ] << "." << endl;
        currentRoom = mapNeighbors[ currentRoom ][ intendedDirection ];
    }
    else if ( mapNeighbors[ currentRoom ][ intendedDirection ] == 128 ) // 2410
    {
        cout << "THE GUARD WON'T LET YOU!" << endl;
        return;
    }
    else
    {
        cout << "YOU CAN'T GO THERE!" << endl;
        return;
    }
}

int GetItemIndex( const string objectNames[], const string objectTags[], const int numberOfObjects, string noun )
{
    int handleObject = 0;

    // Figure out which object we're working with
    for ( int i = 0; i < numberOfObjects; i++ )
    {
        if ( noun == objectNames[i] || noun == objectTags[i] )
        {
            handleObject = i;
            break;
        }
    }
    return handleObject;
}

// Line 2499
void HandleGetCommand( const string objectNames[], const string objectTags[], int objectInRoom[],
    const int numberOfObjects, const int currentRoom, const int maximumInventoryItems,
    int& itemsInInventory, const string verb, const string noun )
{
    int handleObject = GetItemIndex( objectNames, objectTags, numberOfObjects, noun );

    cout << endl;

    if ( handleObject == 0 )
    {
        cout << "YOU CAN'T GET THAT!" << endl;
    }
    else if ( objectInRoom[ handleObject ] == -1 )
    {
        cout << "YOU ALREADY HAVE IT!" << endl;
    }
    else if ( objectInRoom[ handleObject ] > 127 )
    {
        cout << "YOU CAN'T GET THAT!" << endl;
    }
    else if ( objectInRoom[ handleObject ] != currentRoom )
    {
        cout << "THAT'S NOT HERE!" << endl;
    }
    else if ( itemsInInventory + 1 > maximumInventoryItems )
    {
        cout << "YOU CAN'T CARRY ANY MORE." << endl;
    }
    else
    {
        itemsInInventory++;
        objectInRoom[ handleObject ] = -1;
        cout << "TAKEN." << endl;
    }
}

// Line 2599
void HandleDropCommand( string objectNames[], string objectTags[], int objectInRoom[],
    const int numberOfObjects, const int currentRoom,
    int& itemsInInventory, const string verb, const string noun )
{
    int handleObject = GetItemIndex( objectNames, objectTags, numberOfObjects, noun );

    cout << endl;

    if ( handleObject == 0 || objectInRoom[ handleObject ] != -1 )
    {
        // Object doesn't exist or you don't have it
        cout << "YOU DON'T HAVE THAT!" << endl;
    }
    else
    {
        objectInRoom[ handleObject ] = currentRoom;
        itemsInInventory--;
        cout << "DROPPED." << endl;
    }
}

// Line 2699
void HandleInventoryCommand( const string objectNames[], const string objectTags[],
    const int objectInRoom[], const int numberOfObjects )
{
    cout << endl << "YOU ARE CARRYING: ";

    bool carryingAnything = false;
    for ( int i = 0; i < numberOfObjects; i++ )
    {
        if ( objectInRoom[i] == -1 )    // Carrying it
        {
            carryingAnything = true;
            cout << endl << "- " << objectNames[i];
        }
    }

    if ( !carryingAnything )
    {
        cout << "NOTHING.";
    }

    cout << endl;
}

// Line 2799
void HandleLookCommand( const string objectNames[], const string objectTags[],
    int objectInRoom[], const int numberOfObjects, int currentRoom, const string verb, const string noun )
{
    cout << endl;

    int handleObject = GetItemIndex( objectNames, objectTags, numberOfObjects, noun );

    if ( noun == "GROUND" || noun == "GRO" )
    {
        if ( currentRoom == 6 )
        {
            cout << "IT LOOKS LIKE SOMETHING'S BURIED HERE." << endl;
        }
        else
        {
            cout << "IT LOOKS LIKE GROUND!" << endl;
        }
    }

    // Object is either in the room or in our inventory
    if ( IsItemInInventory( objectInRoom, handleObject ) ||
         IsItemInRoom( objectInRoom, handleObject, currentRoom ) )
    {
        if ( handleObject == 7 ) // Bottle
        {
            cout << "THERE'S SOMETHING WRITTEN ON IT!" << endl;
        }
        else if ( handleObject == 15 )
        {
            cout << "THERE'S A JEWEL INSIDE!" << endl;
        }
        else if ( handleObject == 6 )
        {
            cout << "IT'S FILLED WITH RAIN WATER." << endl;
        }
        else
        {
            cout << "YOU SEE NOTHING UNUSUAL." << endl;
        }
    }
    else
    {
        cout << "IT'S NOT HERE!" << endl;
    }
}

void HandleReadCommand( const string objectNames[], const string objectTags[], int objectInRoom[], const int numberOfObjects, int currentRoom, const string verb, const string noun )
{
    cout << endl;

    int handleObject = GetItemIndex( objectNames, objectTags, numberOfObjects, noun );

    // Object is either in the room or in our inventory
    if ( IsItemInInventory( objectInRoom, handleObject ) ||
         IsItemInRoom( objectInRoom, handleObject, currentRoom ) )
    {
        if ( handleObject == 1 ) // Diary
        {
            cout << "IT SAYS: 'ADD SODIUM CHLORIDE PLUS THE" << endl
                << "FORMULA TO RAINWATER, TO REACH THE" << endl
                << "OTHER WORLD.'" << endl;
        }
        else if ( handleObject == 5 ) // Dictionary
        {
            cout << "IT SAYS: SODIUM CHLORIDE IS" << endl
                << "COMMON TABLE SALT." << endl;
        }
        else if ( handleObject == 7 ) // Bottle
        {
            cout << "IT READS: 'SECRET FORMULA'." << endl;
        }
    }
    else
    {
        cout << "YOU CAN'T READ THAT!" << endl;
    }
}

void HandleOtherCommands( map<FLAG_NAME, FLAG_VALUE>& gameFlags,
    int mapNeighbors[19][6],
    const string objectNames[], const string objectTags[],
    int objectInRoom[], const int numberOfObjects,
    int& currentRoom, const string verb, const string noun )
{
    cout << endl;

    int handleObject = GetItemIndex( objectNames, objectTags, numberOfObjects, noun );
    bool isItemInRoom = IsItemInRoom( objectInRoom, handleObject, currentRoom );
    bool isItemInInventory = IsItemInInventory( objectInRoom, handleObject );

    if ( verb == "OPEN" || verb == "OPE" )
    {
        if ( handleObject == 2 ) // Cabinet
        {
            if ( isItemInRoom || isItemInInventory )
            {
                cout << "SOMETHING FELL OUT!" << endl;
                objectInRoom[7] = currentRoom;
            }
            else
            {
                cout << "THERE'S NO BOX HERE!" << endl;
            }
        }
        else if ( handleObject == 3 ) // Cabinet
        {
            if ( isItemInRoom )
            {
                cout << "THERE'S SOMETHING INSIDE!" << endl;
                objectInRoom[4] = 2;
            }
            else
            {
                cout << "THERE'S NO CABINET HERE!" << endl;
            }
        }
        else if ( handleObject == 15 ) // Glass case
        {
            if ( isItemInRoom )
            {
                cout << "THE CASE IS ELECTRIFIED!" << endl;

                if ( gameFlags[ GLOVE_FLAG ] == WEARING )
                {
                    cout << "THE GLOVES INSULATE AGAINST THE" << endl
                        << "ELECTRICITY! THE CASE OPENS!" << endl;
                    objectInRoom[16] = 18;  // Move Ruby to room.
                }
            }
            else
            {
                cout << "THERE'S NO CASE HERE!" << endl;
            }
        }
        else
        {
            cout << "YOU CAN'T OPEN THAT!" << endl;
        }
    }
    else if ( verb == "POUR" || verb == "POU" )
    {
        if ( handleObject == 4 )    // Salt
        {
            if ( !isItemInRoom && !isItemInInventory )
            {
                cout << "YOU DON'T HAVE THE SALT!" << endl;
            }
            else if ( gameFlags[ SALT_POURED ] == POURED )
            {
                cout << "THE SHAKER IS EMPTY!" << endl;
            }
            else
            {
                gameFlags[ SALT_POURED ] = POURED;

                if ( currentRoom == 5 )
                {
                    gameFlags[ BARREL_HAS_SALT ] = HAS;
                    cout << "POURED INTO THE BARREL!" << endl;
                }
                else
                {
                    cout << "POURED!" << endl;
                    SetToNotWinnable( gameFlags );
                }
            }
        }
        else if ( handleObject == 7 )   // Bottle
        {
            if ( !isItemInRoom && !isItemInInventory )
            {
                cout << "YOU DON'T HAVE THE BOTTLE!" << endl;
            }
            else if ( gameFlags[ FORMULA_POURED ] == POURED )
            {
                cout << "THE BOTTLE IS EMPTY!" << endl;
            }
            else
            {
                gameFlags[ FORMULA_POURED ] = POURED;

                if ( currentRoom == 5 )
                {
                    gameFlags[ BARREL_HAS_FORMULA ] = HAS;
                    cout << "POURED INTO THE BARREL!" << endl;
                }
                else
                {
                    cout << "POURED!" << endl;
                    SetToNotWinnable( gameFlags );
                }
            }
        }
    }
    else if ( verb == "CLIMB" || verb == "CLI" )
    {
        if ( handleObject == 10 ) // Tree
        {
            if ( currentRoom != 7 )
            {
                cout << "THERE'S NO TREE HERE!" << endl;
            }
            else
            {
                cout << "YOU CAN'T REACH THE BRANCHES!" << endl;
            }
        }
        else if ( handleObject == 8 )
        {
            if ( !isItemInInventory && !isItemInRoom )
            {
                cout << "YOU DON'T HAVE THE LADDER!" << endl;
            }
            else if ( currentRoom != 7 )
            {
                cout << "THE LADDER SINKS UNDER YOUR WEIGHT!" << endl
                    << "IT DISAPPEARS INTO THE GROUND!" << endl;
                objectInRoom[8] = 0;
            }
            else if ( currentRoom != 20 )
            {
                cout << "WHATEVER FOR?" << endl;
            }
            else
            {
                cout << "IT WON'T DO ANY GOOD." << endl;
            }
        }
    }
    else if ( verb == "JUMP" || verb == "JUM" )
    {
        if ( currentRoom != 7 && currentRoom != 8 )
        {
            cout << "WHEE! THAT WAS FUN!" << endl;
        }
        else if ( currentRoom == 7 )
        {
            cout << "YOU GRAB THE LOWEST BRANCH OF THE" << endl
                << "TREE AND PULL YOURSELF UP..." << endl;

            currentRoom = 8;
        }
        else if ( currentRoom == 8 )
        {
            cout << "YOU GRAB A HIGHER BRANCH ON THE" << endl
                << "TREE AND PULL YOURSELF UP..." << endl;

            currentRoom = 19;
        }
    }
    else if ( verb == "DIG" )
    {
        handleObject = GetItemIndex( objectNames, objectTags, numberOfObjects, "SHO" );
        isItemInRoom = IsItemInRoom( objectInRoom, handleObject, currentRoom );
        isItemInInventory = IsItemInInventory( objectInRoom, handleObject );

        if ( !isItemInInventory && !isItemInRoom )
        {
            cout << "YOU DON'T HAVE A SHOVEL!" << endl;
        }
        else if ( currentRoom != 6 )
        {
            cout << "YOU DON'T FIND ANYTHING." << endl;
        }
        else if ( objectInRoom[11] != 0 )
        {
            cout << "THERE'S NOTHING ELSE THERE!" << endl;
        }
        else
        {
            cout << "THERE'S SOMETHING THERE!" << endl;
            objectInRoom[11] = 6;
        }
    }
    else if ( verb == "ROW" )
    {
        if ( noun != "BOA" && noun != "BOAT" )
        {
            cout << "HOW CAN YOU ROW THAT?" << endl;
        }
        else if ( currentRoom != 13 )
        {
            cout << "YOU'RE NOT IN THE BOAT!" << endl;
        }
        else
        {
            cout << "YOU DON'T HAVE AN OAR!" << endl;
        }
    }
    else if ( verb == "WAVE" || verb == "WAV" )
    {
        if ( handleObject != 13 )   // Fan
        {
            cout << "YOU CAN'T WAVE THAT!" << endl;
        }
        else if ( !isItemInInventory && !isItemInRoom )
        {
            cout << "YOU DON'T HAVE THE FAN!" << endl;
        }
        else if ( currentRoom != 13 )
        {
            cout << "YOU FEEL A REFRESHING BREEZE!" << endl;
        }
        else
        {
            cout << "A POWERFUL BREEZE PROPELS THE BOAT" << endl
                << "TO THE OPPOSITE SHORE!" << endl;
            if ( objectInRoom[12] == 140 )
            {
                objectInRoom[12] = 142;
            }
            else
            {
                objectInRoom[12] = 140;
            }
        }
    }
    else if ( verb == "LEAVE" || verb == "LEA" )
    {
        if ( currentRoom != 13 )
        {
            cout << "PLEASE GIVE A DIRECTION!" << endl;
        }
        else if ( noun != "BOA" && noun != "BOAT" )
        {
            cout << "HUH?" << endl;
        }
        else
        {
            currentRoom = objectInRoom[12] - 128;
        }
    }
    else if ( verb == "FIGHT" || verb == "FIG" )
    {
        if ( noun == "" )
        {
            cout << "WHOM DO YOU WANT TO FIGHT?" << endl;
        }
        else if ( handleObject == 14 )  // guard
        {
            handleObject = GetItemIndex( objectNames, objectTags, numberOfObjects, "SWO" );
            isItemInRoom = IsItemInRoom( objectInRoom, handleObject, currentRoom );
            isItemInInventory = IsItemInInventory( objectInRoom, handleObject );

            if ( !isItemInRoom && !isItemInInventory )
            {
                cout << "YOU CAN'T FIGHT HIM!" << endl;
            }
            else
            {
                cout << "THE GUARD, NOTICING YOUR SWORD," << endl
                    << "WISELY RETREATS INTO THE CASTLE." << endl;

                mapNeighbors[16][NORTH] = 17;
                objectInRoom[14] = 0;
            }
        }
        else if ( currentRoom != 16 )
        {
            cout << "THERE'S NO GUARD HERE!" << endl;
        }
        else if ( objectInRoom[11] != -1 )
        {
            cout << "YOU DON'T HAVE A WEAPON!" << endl;
        }
    }
    else if ( verb == "WEAR" || verb == "WEA" )
    {
        if ( handleObject != 17 )   // Gloves
        {
            cout << "YOU CAN'T WEAR THAT!" << endl;
        }
        else if ( !isItemInInventory && !isItemInRoom )
        {
            cout << "YOU DON'T HAVE THE GLOVES." << endl;
        }
        else
        {
            cout << "YOU ARE NOW WEARING THE GLOVES." << endl;
            gameFlags[ GLOVE_FLAG ] = WEARING;
        }
    }


    // Are the ingredients in the barrel?
    if ( gameFlags[ BARREL_HAS_FORMULA ]    == HAS &&
         gameFlags[ BARREL_HAS_SALT ]       == HAS &&
         gameFlags[ BARREL_HAS_WATER ]      == HAS &&
         gameFlags[ TELEPORT_STATE ]        == NOT_TELEPORTED )
    {
        cout << "THERE IS AN EXPLOSION!" << endl
            << "EVERYTHING GOES BLACK!" << endl
            << "SUDDENLY YOU ARE..." << endl
            << "...SOMEWHERE ELSE!" << endl;
        gameFlags[ TELEPORT_STATE ] = TELEPORTED;

        if ( objectInRoom[9] != -1 ) // shovel not in inventory
        {
            SetToNotWinnable( gameFlags );
        }

        currentRoom = 6;
    }
}

// Line 25000
void SetupNeighbors( int mapNeighbors[19][6], const int numberOfRooms, const int numberOfDirections )
{
    // Initialize all neighbors to 0 (none)
    for ( int r = 0; r < numberOfRooms; r++ )
    {
        for ( int d = 0; d < numberOfDirections; d++ )
        {
            mapNeighbors[r][d] = 0;
        }
    }

    // Map #1's 0 (north) neighbor is Map #4
    mapNeighbors[1][NORTH]  = 4;    // LIVING ROOM
    mapNeighbors[1][SOUTH]  = 3;
    mapNeighbors[1][EAST]   = 2;

    mapNeighbors[2][WEST]   = 1;    // KITCHEN

    mapNeighbors[3][NORTH]  = 1;    // LIBRARY

    mapNeighbors[4][SOUTH]  = 1;    // FRONT YARD
    mapNeighbors[4][WEST]   = 5;

    mapNeighbors[5][EAST]   = 4;    // GARAGE

    mapNeighbors[6][NORTH]  = 9;    // OPEN FIELD
    mapNeighbors[6][SOUTH]  = 7;

    mapNeighbors[7][NORTH]  = 6;    // EDGE OF FOREST

    mapNeighbors[8][DOWN]   = 7;    // BRANCH OF TREE

    mapNeighbors[9][SOUTH]  = 6;    // LONG, WINDING ROAD (1)
    mapNeighbors[9][EAST]   = 10;

    mapNeighbors[10][NORTH]  = 11;   // LONG, WINDING ROAD (2)
    mapNeighbors[10][WEST]   = 9;

    mapNeighbors[11][SOUTH]  = 10;   // LONG, WINDING ROAD (3)
    mapNeighbors[11][WEST]   = 12;

    mapNeighbors[12][EAST]   = 11;   // SOUTH BANK OF RIVER

    // No neighbors for BOAT [13]

    mapNeighbors[14][NORTH]  = 15;  // NORTH BANK OF RIVER

    mapNeighbors[15][NORTH]  = 16;  // WELL-TRAVELED ROAD
    mapNeighbors[15][SOUTH]  = 14;

    mapNeighbors[16][NORTH]  = 128; // SOUTH CASTLE
    mapNeighbors[16][SOUTH]  = 15;

    mapNeighbors[17][UP]     = 18;  // NARROW HALL

    mapNeighbors[18][DOWN]   = 17;  // LARGE HALL

    mapNeighbors[19][DOWN]   = 8; // TOP OF TREE
}

// Line 26000
void SetupObjects( string objectNames[], string objectTags[], int objectInRoom[], const int numberOfObjects )
{
    objectNames[1] = "AN OLD DIARY";                objectTags[1]  = "DIA";     objectInRoom[1]  = 1;
    objectNames[2] = "A SMALL BOX";                 objectTags[2]  = "BOX";     objectInRoom[2]  = 1;
    objectNames[3] = "CABINET";                     objectTags[3]  = "CAB";     objectInRoom[3]  = 130;
    objectNames[4] = "A SALT SHAKER";               objectTags[4]  = "SAL";     objectInRoom[4]  = 0;
    objectNames[5] = "A DICTIONARY";                objectTags[5]  = "DIC";     objectInRoom[5]  = 3;
    objectNames[6] = "WOODEN BARREL";               objectTags[6]  = "BAR";     objectInRoom[6]  = 133;
    objectNames[7] = "A SMALL BOTTLE";              objectTags[7]  = "BOT";     objectInRoom[7]  = 0;
    objectNames[8] = "A LADDER";                    objectTags[8]  = "LAD";     objectInRoom[8]  = 4;
    objectNames[9] = "A SHOVEL";                    objectTags[9]  = "SHO";     objectInRoom[9]  = 5;
    objectNames[10] = "A TREE";                     objectTags[10] = "TRE";     objectInRoom[10] = 135;
    objectNames[11] = "A GOLDEN SWORD";             objectTags[11] = "SWO";     objectInRoom[11] = 0;
    objectNames[12] = "A WOODEN BOAT";              objectTags[12] = "BOA";     objectInRoom[12] = 140;
    objectNames[13] = "A MAGIC FAN";                objectTags[13] = "FAN";     objectInRoom[13] = 8;
    objectNames[14] = "A NASTY-LOOKING GUARD";      objectTags[14] = "GUA";     objectInRoom[14] = 144;
    objectNames[15] = "A GLASS CASE";               objectTags[15] = "CAS";     objectInRoom[15] = 146;
    objectNames[16] = "A GLOWING RUBY";             objectTags[16] = "RUB";     objectInRoom[16] = 0;
    objectNames[17] = "A PAIR OF RUBBER GLOVES";    objectTags[17] = "GLO";     objectInRoom[17] = 19;
}

// Line 27000
void SetupRoomNames( string roomNames[], const int numberOfRooms )
{
    roomNames[0]  = "NONEXISTANCE";
    roomNames[1]  = "IN YOUR LIVING ROOM";
    roomNames[2]  = "IN THE KITCHEN.";
    roomNames[3]  = "IN THE LIBRARY.";
    roomNames[4]  = "IN THE FRONT YARD.";
    roomNames[5]  = "IN THE GARAGE.";
    roomNames[6]  = "IN AN OPEN FIELD.";
    roomNames[7]  = "AT THE EDGE OF A FOREST.";
    roomNames[8]  = "ON A BRANCH OF A TREE.";
    roomNames[9]  = "ON A LONG, WINDING ROAD.";
    roomNames[10] = "ON A LONG, WINDING ROAD.";
    roomNames[11] = "ON A LONG, WINDING ROAD.";
    roomNames[12] = "ON THE SOUTH BANK OF A RIVER.";
    roomNames[13] = "INSIDE THE WOODEN BOAT.";
    roomNames[14] = "ON THE NORTH BANK OF A RIVER.";
    roomNames[15] = "ON A WELL-TRAVELED ROAD.";
    roomNames[16] = "IN FRONT OF A LARGE CASTLE.";
    roomNames[17] = "IN A NARROW HALL.";
    roomNames[18] = "IN A LARGE HALL.";
    roomNames[19] = "ON THE TOP OF A TREE.";
}

// Line 30000
void IntroSequence()
{
}


void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

void HorizontalRule()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void SaveGameState( int& currentRoom, int& itemsInInventory, int objectInRoom[], int numberOfObjects, map<FLAG_NAME, FLAG_VALUE>& gameFlags )
{
    ofstream savegame( "save.txt" );
    savegame << "CURRENT_ROOM       " << currentRoom << endl;
    savegame << "ITEMS_IN_INVENTORY " << itemsInInventory << endl;

    for ( auto& [key, value] : gameFlags )
    {
        savegame << "GAME_FLAG          " << key << " " << value << endl;
    }

    for ( int i = 0; i < numberOfObjects; i++ )
    {
        savegame << "OBJECT_IN_ROOM     " << i << " " << objectInRoom[i] << endl;
    }

    cout << "SAVED GAME." << endl;
}

void LoadGameState( int& currentRoom, int& itemsInInventory, int objectInRoom[], int numberOfObjects, map<FLAG_NAME, FLAG_VALUE>& gameFlags )
{
    ifstream savegame( "save.txt" );

    string option;
    int a, b;
    while ( savegame >> option )
    {
        if ( option == "CURRENT_ROOM" )
        {
            savegame >> currentRoom;
        }
        else if ( option == "ITEMS_IN_INVENTORY" )
        {
            savegame >> itemsInInventory;
        }
        else if ( option == "GAME_FLAG" )
        {
            savegame >> a >> b;
            gameFlags[FLAG_NAME(a)] = FLAG_VALUE(b);
        }
        else if ( option == "OBJECT_IN_ROOM" )
        {
            savegame >> a >> b;
            objectInRoom[a] = b;
        }
    }

    cout << "LOADED." << endl;
}

#endif
