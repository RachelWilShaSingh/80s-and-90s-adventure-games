/* The Quest
From Christopher Lampton's
How to Create Adventure Games book
Published in 1986
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <map>
using namespace std;

#include "Functions.hpp"

int main()
{
    const int numberOfRooms = 20;       // NR
    const int numberOfDirections = 6;   // ND
    const string directions[6] = {
        "NORTH", "SOUTH", "EAST",
        "WEST", "UP", "DOWN"
    };
    const int numberOfObjects = 18;     // NO
    const int maximumInventoryItems = 5; // NI

    int currentRoom = 1; // R
    int itemsInInventory = 0;   // IN

    string roomNames[ numberOfRooms ]; // R$
    int mapNeighbors[ numberOfRooms ][ numberOfDirections ]; // MA

    string objectNames[ numberOfObjects ];   // OB$
    string objectTags[ numberOfObjects ];    // O2$
    int objectInRoom[ numberOfObjects ];     // OB

    string verb, noun;
    int directionIntended; // DI

    map<FLAG_NAME, FLAG_VALUE> gameFlags;
    gameFlags[ GLOVE_FLAG ] = NOT_WEARING;
    gameFlags[ SALT_POURED ] = NOT_POURED;
    gameFlags[ FORMULA_POURED ] = NOT_POURED;
    gameFlags[ BARREL_HAS_WATER ] = HAS;
    gameFlags[ BARREL_HAS_SALT ] = NOT_HAVE;
    gameFlags[ BARREL_HAS_FORMULA ] = NOT_HAVE;
    gameFlags[ WINNABLE_STATE ] = WINNABLE;
    gameFlags[ TELEPORT_STATE ] = NOT_TELEPORTED;

    SetupNeighbors( mapNeighbors, numberOfRooms, numberOfDirections );
    SetupObjects( objectNames, objectTags, objectInRoom, numberOfObjects );
    SetupRoomNames( roomNames, numberOfRooms );

    // Introduction
    cout << "ALL YOUR LIFE YOU HAD HEARD THE STORIES" << endl
        << "ABOUT YOUR CRAZY UNCLE SIMON. HE WAS AN" << endl
        << "INVENTOR, WHO KEPT DISAPPEARING FOR" << endl
        << "LONG PERIODS OF TIME, NEVER TELLING" << endl
        << "ANYONE WHERE HE HAD BEEN." << endl
        << endl
        << "YOU NEVER BELIEVE THE STORIES, BUT" << endl
        << "WHEN YOUR UNCLE DIED AND LEFT YOU HIS" << endl
        << "DIARY, YOU LEARNED THAT THEY WERE TRUE." << endl
        << "YOUR UNCLE HAD DISCOVERED A MAGIC" << endl
        << "LAND, AND A SECRET FORMULA THAT COULD" << endl
        << "TAKE HIM THERE. IN THAT LAND WAS A" << endl
        << "MAGIC RUBY, AND HIS DIARY CONTAINED" << endl
        << "THE INSTRUCTIONS FOR GOING THERE TO" << endl
        << "FIND IT." << endl << endl;

    bool done = false;
    while ( !done )
    {
        HorizontalRule();
        PrintRoomDescription( roomNames, currentRoom );
        PrintWhatYouCanSee( objectNames, objectTags, objectInRoom, numberOfObjects, currentRoom );
        PrintDirectionsYouCanGo( mapNeighbors, directions, numberOfRooms, numberOfDirections, currentRoom );

        GetInput( verb, noun );

        if ( noun == "SHA" || noun == "SHAKER" )
        {
            noun = "SALT";
        }
        else if ( noun == "FOR" || noun == "FORMULA" )
        {
            noun = "BOTTLE";
        }

        if ( verb == "GO" )
        {
            HandleGoCommand( mapNeighbors, directions, objectInRoom, currentRoom, verb, noun );
        }
        else if ( verb == "GET" || verb == "TAKE" )
        {
            HandleGetCommand( objectNames, objectTags, objectInRoom, numberOfObjects, currentRoom, maximumInventoryItems, itemsInInventory, verb, noun );
        }
        else if ( verb == "DROP" )
        {
            HandleDropCommand( objectNames, objectTags, objectInRoom, numberOfObjects, currentRoom, itemsInInventory, verb, noun );
        }
        else if ( verb == "INVENTORY" )
        {
            HandleInventoryCommand( objectNames, objectTags, objectInRoom, numberOfObjects );
        }
        else if ( verb == "LOOK" || verb == "LOO" || verb == "EXAMINE" || verb == "EXA" )
        {
            HandleLookCommand( objectNames, objectTags, objectInRoom, numberOfObjects, currentRoom, verb, noun );
        }
        else if ( verb == "READ" )
        {
            HandleReadCommand( objectNames, objectTags, objectInRoom, numberOfObjects, currentRoom, verb, noun );
        }
        else if ( verb == "OPEN" || verb == "OPE" ||
                  verb == "POUR" || verb == "POU" ||
                  verb == "CLIMB" || verb == "CLI" ||
                  verb == "JUMP" || verb == "JUM" ||
                  verb == "DIG" ||
                  verb == "ROW" ||
                  verb == "WAVE" || verb == "WAV" ||
                  verb == "LEAVE" || verb == "LEA" ||
                  verb == "FIGHT" || verb == "FIG" ||
                  verb == "WEAR" || verb == "WEA" )
        {
            HandleOtherCommands( gameFlags, mapNeighbors, objectNames, objectTags, objectInRoom, numberOfObjects, currentRoom, verb, noun );
        }
        else if ( verb == "QUIT" )
        {
            cout << "ARE YOU SURE YOU WANT TO QUIT (Y/N): ";
            string reallyQuit;
            getline( cin, reallyQuit );
            reallyQuit = ToUpper( reallyQuit );

            if ( reallyQuit == "YES" || reallyQuit == "Y" )
            {
                done = true;
            }
        }
        else if ( verb == "DEBUG" )
        {
            cout << "DEBUG -----------------------" << endl;
            cout << "currentRoom        = " << currentRoom << endl;
            cout << "itemsInInventory   = " << itemsInInventory << endl;

            cout << endl << "OBJECT LOCATIONS -----------" << endl;
            for ( int i = 0; i < numberOfObjects; i++ )
            {
                int objInRoom = objectInRoom[i];

                if ( objInRoom >= 128 )
                {
                    objInRoom -= 128;
                }

                cout << left << setw( 5 ) << "- " << i << ": "
                     << setw( 5 ) << objectTags[i]
                     << setw( 8 ) << " In room "
                     << setw( 5 ) << objectInRoom[i] << "(";

                if ( objInRoom >= 0 && objInRoom < numberOfRooms )
                {
                    cout << setw( 20 ) << roomNames[ objInRoom ] << ")" << endl;
                }
                else if ( objInRoom == -1 )
                {
                    cout << setw( 20 ) << "INVENTORY)" << endl;
                }
            }

            cout << endl << "GAME FLAGS ------------------" << endl;
            cout << "VALUES:" << endl;
            cout << "NOT_WEARING = " << NOT_WEARING << ", WEARIING = " << WEARING << endl;
            cout << "NOT_POURED = " << NOT_POURED << ", POURED = " << POURED << endl;
            cout << "HAS = " << HAS << ", NOT_HAVE = " << NOT_HAVE << endl;
            cout << "WINNABLE = " << WINNABLE << ", NOT_WINNABLE = " << NOT_WINNABLE << endl;
            cout << "TELEPORTED = " << TELEPORTED << ", NOT_TELEPORTED = " << NOT_TELEPORTED << endl;
            cout << endl << "FLAGS:" << endl;
            cout << "gameFlags[ GLOVE_FLAG ]            = " << gameFlags[ GLOVE_FLAG ] << endl;
            cout << "gameFlags[ SALT_POURED ]           = " << gameFlags[ SALT_POURED ] << endl;
            cout << "gameFlags[ FORMULA_POURED ]        = " << gameFlags[ FORMULA_POURED ] << endl;
            cout << "gameFlags[ BARREL_HAS_WATER ]      = " << gameFlags[ BARREL_HAS_WATER ] << endl;
            cout << "gameFlags[ BARREL_HAS_SALT ]       = " << gameFlags[ BARREL_HAS_SALT ] << endl;
            cout << "gameFlags[ BARREL_HAS_FORMULA ]    = " << gameFlags[ BARREL_HAS_FORMULA ] << endl;
            cout << "gameFlags[ WINNABLE_STATE ]        = " << gameFlags[ WINNABLE_STATE ] << endl;
            cout << "gameFlags[ TELEPORT_STATE ]        = " << gameFlags[ TELEPORT_STATE ] << endl;
        }
        else if ( verb == "SAVE" )
        {
            SaveGameState( currentRoom, itemsInInventory, objectInRoom, numberOfObjects, gameFlags );
        }
        else if ( verb == "LOAD" )
        {
            LoadGameState( currentRoom, itemsInInventory, objectInRoom, numberOfObjects, gameFlags );
        }
        else
        {
            cout << "I DON'T KNOW HOW TO DO THAT" << endl;
        }

        if ( objectInRoom[ 16 ] == -1 ) // If player picked up the ruby
        {
            cout << "CONGRATULATIONS! YOU'VE WON!" << endl;
            done = true;
        }
    }

    return 0;
}
