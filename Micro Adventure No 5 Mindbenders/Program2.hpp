#ifndef _PROGRAM2
#define _PROGRAM2

#include <iostream>
#include <string>
using namespace std;

void ClearScreen();
void Pause();

void Program2()
{
    int right = 0;  // R
    int M = 0;  // M
    string data[] = {   "GOOD", "EVIL",
                        "LIFE", "DEATH",
                        "ANGEL", "DAMIAN",
                        "SOFT", "HARD",
                        "ACT", "BRUTE",
                        "POLICEMAN", "CRIMINAL",
                        "PASS", "FLUNK",
                        "HEAVEN", "HALE",
                        "ORDER", "CHAOS",
                        "TRUTH", "LIES" };

    string question; // Q$
    string answer;   // A$
    string userAnswer; // W$
    int k = 0;
    for ( k = 0; k < 10; k++ )
    {
        int questionIndex = 2 * k;
        int answerIndex = 2 * k + 1;
        cout << "Question " << k << endl;
        question = data[questionIndex];
        answer = data[answerIndex];

        //ClearScreen();

        cout << "What does " << question << " make you think of?" << endl;
        cin >> userAnswer;

        if ( userAnswer == answer )
        {
            cout << "VERY GOOD" << endl;
            right = right + 1;
        }
        else
        {
            cout << "WRONG" << endl;
        }

        for ( int l = 1; l <= 500; l++ )
        {
            cout << char( l );
        }

        cout << endl << endl;
    }

    cout << "YOUR SCORE IS " << right << " OUT OF 10" << endl;

    if ( right < k )
    {
        cout << "ANOTHER ONE BITS THE DUST" << endl;
    }
    else
    {
        cout << "THIS ONE IS UNDER CONTROL" << endl;
    }
}

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}


void Pause()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "pause" );
    #else
        cout << endl << " Press ENTER to continue..." << endl;
        cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
    #endif
}

#endif


