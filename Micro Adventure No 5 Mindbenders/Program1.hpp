#ifndef _PROGRAM1
#define _PROGRAM1

#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    string normalAlphabet  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ .";     // B
    string encodedAlphabet = "SUNABCDEFGHIJKLMOPQRTVWXYZ .";   // A
    string encodedMessage; // M
    char runAgain = 'Y';

    while ( toupper( runAgain ) == 'Y' )
    {
        string decodedMessage = "";

        cout << "Enter line of message: ";
        encodedMessage = "TPDBKR LPKFLK AL KLREFKD RL QRLM REFQ EFGSNHFKD";
        //getline( cin, encodedMessage );

        for ( int i = 0; i < encodedMessage.size(); i++ )
        {
            for (int  k = 0; k < encodedAlphabet.size(); k++ )
            {
                if ( encodedMessage[i] == encodedAlphabet[k] )
                {
                    decodedMessage = decodedMessage + normalAlphabet[k];
                }
            }
        }

        cout << "Encoded message: " << endl << encodedMessage << endl;
        cout << "Decoded message: " << endl << decodedMessage << endl;
        cout << endl;
        cout << "More messages to decode? Y or N: ";
        cin >> runAgain;
        cin.ignore();

        cout << endl << endl;
    }
}

#endif
